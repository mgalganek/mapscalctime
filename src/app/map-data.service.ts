import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'

@Injectable()
export class MapDataService {
  mapsKey: string = ''
  mapsAPI: string = 'https://maps.googleapis.com/maps/api/distancematrix/json';

  constructor(private http: Http) { }

  calculateDistanceAndTime(
    {orgins, destinations} : {orgins: string, destinations: string}) :
    Observable<Object> {
    orgins = this.textToString(orgins);
    destinations = this.textToString(destinations);
    let url = `${this.mapsAPI}?origins=${orgins}&destinations=${destinations}`;
    if (this.mapsKey) {
      url += `&apiKey=${this.mapsKey}`;
    }
    return this.http.get(url)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Promise.reject(errMsg);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || { };
  }

  private textToString(str) {
    return str.split(' ').join('+');
  }
}
