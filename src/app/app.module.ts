import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CalcTimeComponent } from './calc-time/calc-time.component';
import { MapDataService } from './map-data.service';

@NgModule({
  declarations: [
    AppComponent,
    CalcTimeComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  providers: [MapDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
