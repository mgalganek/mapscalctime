import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';

import { MapDataService } from "../map-data.service";

@Component({
  selector: 'app-calc-time',
  templateUrl: './calc-time.component.html',
  styleUrls: ['./calc-time.component.css']
})
export class CalcTimeComponent implements OnInit {
  response: Object;

  constructor(public mds: MapDataService) { }

  ngOnInit() {
  }

  makeCalculations(form: NgForm, event: Event) {
    event.preventDefault();
    this.mds.calculateDistanceAndTime({
      orgins: form.value.orgins,
      destinations: form.value.destinations
    }).subscribe((res) => {
      this.response = res;
    }, (error) => {
      console.log(error)
    });
  }

}
