import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcTimeComponent } from './calc-time.component';

describe('CalcTimeComponent', () => {
  let component: CalcTimeComponent;
  let fixture: ComponentFixture<CalcTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
